package com.in28minutes.controller;

import java.util.List;

import com.in28minutes.model.Todo;
import com.in28minutes.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TodoRestController {
	@Autowired
	private TodoService todoService;

	@RequestMapping(value = "/todos", method = RequestMethod.GET)
	public List<Todo> listAllTodos() {
		List<Todo> users = todoService.retrieveTodos( "in28Minutes" );
		return users;
	}

	@RequestMapping(value = "/todo/{id}", method = RequestMethod.GET)
	public Todo retrieveTodo(@PathVariable("id") int id) {
		return todoService.retrieveTodo(id);
	}
}
