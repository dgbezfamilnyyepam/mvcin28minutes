package com.in28minutes.model;

import java.util.Date;

import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Todo {
	private int id;
	private String user;
	@Size(min = 6, message = "Enter atleast 6 characters")
	private String desc;
	private Date targetDate;
	private boolean isDone;
}
